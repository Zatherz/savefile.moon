local fs = require("minifs")
local bxor
bxor = function(a, b)
  local r = 0
  for i = 0, 31 do
    local x = a / 2 + b / 2
    if x ~= math.floor((x)) then
      r = r + 2 ^ i
    end
    a = math.floor((a / 2))
    b = math.floor((b / 2))
  end
  return r
end
local secret = "Putting in a super basic encryption pass so our saves are a little harder to edit than just opening a text or hex editor.  Need a secret key or some such... so here's some nonsense.\n"
local version = "0"
if (not arg[1] and not arg[2]) then
  print("Usage: savefile.lua <input> <output>")
  print("No pretty printing is done on the output JSON")
  return 
end
local filepath = arg[1]
print("File: " .. filepath)
assert((fs.exists(filepath, "File doesn't exist :/")))
local content = fs.read(filepath)
local isjson = (content:sub(1, 1)) == "{"
if (isjson) then
  print("Operation: JSON -> save")
else
  print("Operation: save -> JSON")
end
local outfile = arg[2]
if (not outfile) then
  if (isjson) then
    print("No output file, assuming SaveFile.save")
    outfile = "SaveFile.save"
  else
    print("No output file, assuming SaveFile.json")
    outfile = "SaveFile.json"
  end
end
print("Output: " .. outfile)
local outfilehandler = io.open(outfile, "w")
if (not outfilehandler) then
  error("Failed creating output file handler")
end
if (not isjson) then
  local versionline = ""
  for i = 1, #content do
    local c = content:sub(i, i)
    if (c == "\n") then
      break
    end
    versionline = versionline .. c
  end
  version = versionline:sub(10)
end
print(("Version: " .. version))
local startat = 12
if (isjson) then
  startat = 1
end
local readycontent = content:sub(startat)
local sub0
sub0 = function(self, i, j)
  return self:sub(i + 1, j + 1)
end
if (isjson) then
  outfilehandler:write("version: " .. version .. "\n")
end
if (isjson) then
  print("Encoding...")
else
  print("Decoding...")
end
for i = 0, #readycontent - 1 do
  local c = readycontent:sub(i + 1, i + 1)
  local secretcharposition = i % (#secret - 1)
  local secretbyte = string.byte((sub0(secret, secretcharposition, secretcharposition)))
  local savebyte = string.byte((c))
  outfilehandler:write(string.char(bxor(secretbyte, savebyte)))
end
return outfilehandler:close()
