#!/usr/bin/moon
fs = require "minifs"

-- THE GUNGEON SAVE FORMAT
-- The Gungeon saves are JSON data XORed with a special string ('secret')
-- A version string ('version: 0\n') is prepended to the resulting data
-- To decode, you have to skip the entire version string, then iterate
-- over each character and XOR it with character (iteration % length of secret)
-- of the secret string.

-- Pure Lua bitwise XOR - thanks to Reuben Thomas
-- http://lua-users.org/wiki/BitUtils
bxor =(a,b)->
  r = 0
  for i = 0, 31 do
    x = a / 2 + b / 2
    if x ~= math.floor (x)
      r = r + 2^i
    a = math.floor (a / 2)
    b = math.floor (b / 2)
  return r


secret = "Putting in a super basic encryption pass so our saves are a little harder to edit than just opening a text or hex editor.  Need a secret key or some such... so here's some nonsense.\n"

version = "0"

if (not arg[1] and not arg[2])
  print("Usage: savefile.lua <input> <output>")
  print("No pretty printing is done on the output JSON")
  return

filepath = arg[1]
print("File: " .. filepath)

assert (fs.exists filepath, "File doesn't exist :/")

content = fs.read filepath

isjson = (content\sub 1, 1) == "{"

if (isjson)
  print("Operation: JSON -> save")
else
  print("Operation: save -> JSON")

outfile = arg[2]
if (not outfile)
  if (isjson)
    print("No output file, assuming SaveFile.save")
    outfile = "SaveFile.save"
  else
    print("No output file, assuming SaveFile.json")
    outfile = "SaveFile.json"

print("Output: " .. outfile)

outfilehandler = io.open outfile, "w"
if (not outfilehandler)
  error("Failed creating output file handler")

---- print the version
if (not isjson)
  versionline = ""
  for i = 1, #content
    c = content\sub(i, i)
    if (c == "\n")
      break
    versionline ..= c
  version = versionline\sub(10)

print ("Version: " .. version)

---- ignore the first few bytes (version: NUM\n)
startat = 12
if (isjson)
  startat = 1
readycontent = content\sub startat

sub0 =(i, j)=> self\sub(i + 1, j + 1)

if (isjson)
  outfilehandler\write "version: " .. version .. "\n"

---- decrypt
if (isjson)
  print("Encoding...")
else
  print("Decoding...")
for i = 0, #readycontent - 1
  c = readycontent\sub(i+1, i+1)
  secretcharposition = i % (#secret-1)

  secretbyte = string.byte (sub0 secret, secretcharposition, secretcharposition)
  savebyte = string.byte (c)

  outfilehandler\write string.char bxor(secretbyte, savebyte)
outfilehandler\close!
