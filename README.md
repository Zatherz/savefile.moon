# savefile.moon

Savefile.moon is a prototype Gungeon save file encoder/decoder written in Moonscript.

# How do you use it?

First you have to install Lua. The script has been tested on Lua 5.1, Lua 5.2, Lua 5.3 and LuaJIT.  
LuaJIT is recommended as compared to the others it's insanely fast.

After installing, run `luajit savefile.moon` for usage info. Replace `luajit` with the interpreter if  
you aren't using LuaJIT.

# How to edit it?

Modify the Moonscript files then compile them into Lua with the Moonscript compiler.

# Is there any graphical interface?

Not yet, as this is only a prototype. You're welcome to base off the code and create your own.
